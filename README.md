This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started
We need a node version superior to the 12.22.0 to run the project (NextJS only works with this or higher node versions. I run the 16.15.1 the node LTS)
```
nvm install 16.15.1
nvm use 16.15.1
```

Configure the environment keys:
Rename the file __.env.example__ to __.env__ and fill the necesary keys
To configure the local database you will need to fill the MONGO_URL variable.The Mongo DB local URL should be: __mongodb://localhost:27017/articlesdb__ for local development.

To be able to run de app, we need to have the database up and running
```
docker-compose up -d
```

Install the dependencies:
```bash
npm install
# or
yarn install
```

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

If you need data to fill the database, you can use the endpoint.You can call it from Postman. 
```
http://localhost:3000/api/seed
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.