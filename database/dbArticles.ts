import {Article, ArticleInterface} from '../models';
import {db} from '.';

export const getEntryById =
    async (id: string): Promise<ArticleInterface | null> => {
      await db.connect();
      const entry = await Article.findOne({_id: id}).lean();
      await db.disconnect();
      return entry;
    };
