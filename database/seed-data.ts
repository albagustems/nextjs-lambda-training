interface SeedData {
  articles: SeedArticle[];
}

interface SeedArticle {
    title: string;
    author: string;
    category: string;
    description: string;
    body: string;
    createdAt: number;
}

export const seedData: SeedData = {
  articles: [
    {
      title: 'First Post',
      author: 'John Doe',
      category: 'Test',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
      body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce et elementum turpis. Nullam commodo porttitor ipsum quis cursus. Fusce molestie erat nibh, vel eleifend dui semper et. Quisque gravida, ipsum nec congue ultricies, urna turpis ultrices ex, vel rhoncus ante magna vel elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce aliquet ornare nulla. Integer mollis orci a leo aliquet, nec luctus est pulvinar. Proin in enim eget purus laoreet euismod. Morbi rhoncus nisi nec mauris fringilla, sed semper nibh posuere',      
      createdAt: Date.now(),
    },
    {
      title: 'Second Post',
      author: 'John Doe',
      category: 'Test',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
      body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce et elementum turpis. Nullam commodo porttitor ipsum quis cursus. Fusce molestie erat nibh, vel eleifend dui semper et. Quisque gravida, ipsum nec congue ultricies, urna turpis ultrices ex, vel rhoncus ante magna vel elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce aliquet ornare nulla. Integer mollis orci a leo aliquet, nec luctus est pulvinar. Proin in enim eget purus laoreet euismod. Morbi rhoncus nisi nec mauris fringilla, sed semper nibh posuere',      
      createdAt: Date.now(),
    },
    {
      title: 'Third Post',
      author: 'John Doe',
      category: 'Test',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
      body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce et elementum turpis. Nullam commodo porttitor ipsum quis cursus. Fusce molestie erat nibh, vel eleifend dui semper et. Quisque gravida, ipsum nec congue ultricies, urna turpis ultrices ex, vel rhoncus ante magna vel elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce aliquet ornare nulla. Integer mollis orci a leo aliquet, nec luctus est pulvinar. Proin in enim eget purus laoreet euismod. Morbi rhoncus nisi nec mauris fringilla, sed semper nibh posuere',      
      createdAt: Date.now(),
    },
    {
      title: 'Forth Post',
      author: 'John Doe',
      category: 'Test',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
      body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce et elementum turpis. Nullam commodo porttitor ipsum quis cursus. Fusce molestie erat nibh, vel eleifend dui semper et. Quisque gravida, ipsum nec congue ultricies, urna turpis ultrices ex, vel rhoncus ante magna vel elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce aliquet ornare nulla. Integer mollis orci a leo aliquet, nec luctus est pulvinar. Proin in enim eget purus laoreet euismod. Morbi rhoncus nisi nec mauris fringilla, sed semper nibh posuere',      
      createdAt: Date.now(),
    },
    {
      title: 'Fifth Post',
      author: 'John Doe',
      category: 'Test',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
      body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce et elementum turpis. Nullam commodo porttitor ipsum quis cursus. Fusce molestie erat nibh, vel eleifend dui semper et. Quisque gravida, ipsum nec congue ultricies, urna turpis ultrices ex, vel rhoncus ante magna vel elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce aliquet ornare nulla. Integer mollis orci a leo aliquet, nec luctus est pulvinar. Proin in enim eget purus laoreet euismod. Morbi rhoncus nisi nec mauris fringilla, sed semper nibh posuere',      
      createdAt: Date.now(),
    },
  ],
};
