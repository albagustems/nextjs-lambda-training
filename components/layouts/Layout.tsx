import { Box } from '@mui/material';
import Head from 'next/head';
import React from 'react'
import { Header } from '../ui';

interface Props {
    title?: string,
    children: React.ReactNode
}

export const Layout: React.FC<Props> = ({title = 'Nextium', children}) => {
  return (
    <Box sx={{flexGrow: 1}}>
      <Head>
        <title>{ title }</title>
      </Head>
      
      <Header/>

      <Box sx={{padding: '10px 20px'}}>
        { children }
      </Box>
    </Box>
  );
};
