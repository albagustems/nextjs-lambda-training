import { Box } from '@mui/material'

interface Props {
    image: string,
}

const PostImage: React.FC<Props> = ({image}) => {
  return (
    <Box sx={{ 
        height: '250px', 
        backgroundImage:`url(${image})`, 
        backgroundSize: 'cover', 
        backgroundRepeat: 'no-repeat', 
        backgroundPosition: 'center',
        marginX: '-20px'
    }}>
    </Box>
  )
}

export default PostImage;
