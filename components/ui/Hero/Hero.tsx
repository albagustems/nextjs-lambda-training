import Image from 'next/image'
import { Box, Typography } from '@mui/material'
import styles from './Hero.module.css'

export const Hero = () => {
  return (
    <section className={styles.hero}>
        <Box mr={5}>
            <Typography variant="h2">
                <u>Nextium</u> is a place to write, read, and connect
            </Typography>
            <Typography variant="subtitle1" mt={2}>
                It is easy and free to post your thinking on any topic and connect
                with millions of readers.
            </Typography>
        </Box>
        <Box sx={{ display: { xs: 'none', sm: 'block' }}} alignItems='center' width={300}>
            <Image width={300} height={300} layout='responsive' src='/N.png' alt='Nextium'/>
        </Box>
    </section>
  )
}

