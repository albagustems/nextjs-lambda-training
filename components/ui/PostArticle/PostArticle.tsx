import { Avatar, Box, Typography } from '@mui/material';
import Image from 'next/image';
import styles from './PostArticle.module.css'

interface Props {
    title: string,
    description: string,
    body: string,
    author: string,
    image: string,
}

const PostArticle: React.FC<Props> = ({title, description, body, author, image}) => {
  return (
    <article className={styles.article}>
        <h1>{title}</h1>
        <h3 className={styles['article__subtitle']}>{description}</h3>
        <Box sx={{ display: 'flex', alignItems: 'center', marginBottom: '50px' }}>
            <Avatar sx={{ bgcolor: '#FFC018', marginRight: '20px' }}>{author[0].toUpperCase()}</Avatar>
            <Typography sx={{ color: 'gray' }}>Post created by {author}</Typography>
        </Box>
        <section>{body}</section>
        <Box sx={{ display: 'flex', justifyContent:'center' }}>
            <Image alt={`${title}-post`} src={image} width={650} height={400} layout='fixed' />
        </Box>
    </article>
  )
}

export default PostArticle;