import { Box, Button, ButtonProps, FormControl, TextField } from '@mui/material'
import { styled } from '@mui/material/styles';
import styles from './PostComments.module.css'

const CustomTextField = styled(TextField)({
    marginBottom: '20px',
    '& label.Mui-focused': {
      color: '#FFC018',
    },
    '& .MuiInput-underline:after': {
      borderBottomColor: '#FFC018',
    },
    '& .MuiOutlinedInput-root': {
      '& fieldset': {
        borderColor: 'gray',
      },
      '&:hover fieldset': {
        borderColor: '#FFC018',
      },
      '&.Mui-focused fieldset': {
        borderColor: '#FFC018',
      },
    },
});

const ColorButton = styled(Button)<ButtonProps>(() => ({
    color: 'white',
    textTransform: 'capitalize',
    backgroundColor: '#FFC018',
    '&:hover': {
      backgroundColor: '#ca9a15',
    },
}));

const PostComments = () => {
  return (
    <>
    <hr className={styles.separator}/>
    <Box sx={{ marginX: { xs: '10px', sm: '100px'}, marginY: '50px' }}>
        <section>
            <h6 className={styles['call-to-action']}>Enjoyed this article?</h6>
            <h4 className={styles['comment']}>Leave a comment below!</h4>
        </section>
        
        <FormControl fullWidth>
            <CustomTextField 
                id="comments-name"
                className={styles['form__input']} 
                label="Name" 
                variant="outlined"
                InputLabelProps={{
                    shrink: true,
                }}
                fullWidth
                required
            />
            <CustomTextField 
                id="comments-email"
                className={styles['form__input']} 
                label="Email" 
                variant="outlined"
                InputLabelProps={{
                    shrink: true,
                }}
                fullWidth
                required
            />
            <CustomTextField 
                id="comments-comment"
                className={styles['form__input']} 
                label="Comment" 
                variant="outlined"
                InputLabelProps={{
                    shrink: true,
                }}
                multiline
                rows={6}
                fullWidth
                required
            />
            <ColorButton type='submit'>Submit</ColorButton>
            </FormControl>  
        </Box>
    </>
  )
}

export default PostComments