import { Box, Link } from '@mui/material';
import Image from 'next/image';
import NextLink from 'next/link';
import styles from './Header.module.css'

export const Header = () => {
  return (
    <header className={styles.header}>
        <div className={styles['header_links']}>
        <Box sx={{ cursor: 'pointer' }}>
            <NextLink href='/'>
                <Image alt='logo' src='/logo.png' height={40} width={120} />
            </NextLink>
        </Box>
        <Box sx={{ display: { xs: 'none', sm: 'block' }}}>
          <NextLink href='/about' passHref>
            <Link 
              className={styles['link-header']} 
              color='black' 
              underline='none' 
              variant='body1'> 
              About 
            </Link>
          </NextLink>
          <NextLink href='/contact' passHref>
            <Link 
              className={styles['link-header']} 
              underline='none' 
              color='black' 
              variant='body1'> 
              Contact 
            </Link>
          </NextLink>
          <NextLink href='/follow' passHref>
            <Link 
              className={`${styles['link-header']} ${styles['link-header--green']}` } 
              underline='none' 
              color='white' 
              variant='body1'> 
              Follow 
            </Link>
          </NextLink>
        </Box>
        </div>
        <Box sx={{ display: 'flex', alignItems: 'center'}}>
          <NextLink href='/sign-in' passHref>
            <Link 
              className={styles['link-header']} 
              underline='none' 
              color='green' 
              variant='body1'> 
              Sign in 
            </Link>
          </NextLink>
          <NextLink href='/start' passHref>
            <Link 
              className={`${styles['link-header']} ${styles['link-header--white']}` } 
              underline='none' 
              color='green' 
              variant='body1'> 
              Get Started 
            </Link>
          </NextLink>
        </Box>
    </header>
  )
}
