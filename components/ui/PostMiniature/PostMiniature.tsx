import { Avatar, Box, Typography } from '@mui/material';
import Image from 'next/image';
import NextLink from 'next/link';

interface Props {
    id: string,
    title: string,
    description: string,
    author: string,
}

const PostMiniature: React.FC<Props> = ({id, title, description, author}) => {
  return (
    <NextLink href={`/${id}`} passHref>
        <Box sx={{ 
            border: '1px solid rgba(0, 0, 0, 0.08)', 
            cursor: 'pointer',
            transition: 'all 1s ease-in-out',
            '&:hover': {
                transform: 'scale(1.03)'
            }}}>
            <Image alt={`post-${title}`} src="/blog-image.jpeg" width={300} height={200} layout='responsive'/>
            <Box sx={{ display: 'flex', alignItems: 'center', padding: '10px' }}>
                <section>
                    <Typography variant='body1'>{title}</Typography>
                    <Typography variant='body2'>{description} by {author}</Typography>
                </section>
                <aside>
                    <Avatar sx={{ bgcolor: '#FFC018' }}>N</Avatar>
                </aside>
            </Box>
        </Box>
    </NextLink>
  )
}

export default PostMiniature