export interface Article {
    _id: string;
    title: string;
    author: string;
    description: string;
    category: string;
    body: string;
    createdAt: number;
}

