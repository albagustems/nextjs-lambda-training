import mongoose, {Model, Schema} from 'mongoose';
import {v4 as uuidv4} from 'uuid';
import {Article} from '../interfaces';

export interface ArticleInterface extends Article {}

const articleSchema = new Schema({
  _id: {type: String, default: uuidv4},
  title: {type: String, required: true},
  author: {type: String, required: true},
  category: {type: String, required: true},
  body: {type: String, required: true},
  createdAt: {type: Number, default: Date.now()},
});

const ArticleModel: Model<ArticleInterface> =
    mongoose.models.Article || mongoose.model('Article', articleSchema);

export default ArticleModel;
