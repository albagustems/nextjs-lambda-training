import { Box, Grid } from '@mui/material'
import type { NextPage } from 'next'
import { Layout } from '../components/layouts'
import { Hero } from '../components/ui'
import PostMiniature from '../components/ui/PostMiniature/PostMiniature';

const Home: NextPage = () => {
  return (
    <Layout>
      <Hero/>
      {/* Substitute the Box with Grid and it's content for ListPostsFeature */}
      <Box sx={{ marginY: '10px', marginX: '20px' }}>
        <Grid sx={{ maxWidth: '1536px' }} container spacing={2}>
          <Grid item xs={12} sm={6} md={4} xl={3}>
            <PostMiniature 
              id='1' 
              description='Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
              title='Test' 
              author='John Doe'
              />
          </Grid>
          <Grid item xs={12} sm={6} md={4} xl={3}>
            <PostMiniature 
              id='1' 
              description='Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
              title='Test' 
              author='John Doe'
              />
          </Grid>
          <Grid item xs={12} sm={6} md={4} xl={3}>
            <PostMiniature 
              id='1' 
              description='Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
              title='Test' 
              author='John Doe'
              />
          </Grid>
          <Grid item xs={12} sm={6} md={4} xl={3}>
            <PostMiniature 
              id='1' 
              description='Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
              title='Test' 
              author='John Doe'
              />
          </Grid>
        </Grid>
      </Box>
    </Layout>
  )
}

export default Home
