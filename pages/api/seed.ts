import type {NextApiRequest, NextApiResponse} from 'next';
import {db} from '../../database';
import {seedData} from '../../database';
import {Article} from '../../models';

type Data = {
    message: string
}

export default async function handler(
    req: NextApiRequest, res: NextApiResponse<Data>,
) {
  if (process.env.NODE_ENV === 'production') {
    return res.status(401).json({message: 'Unauthorized service access'});
  }

  await db.connect();

  await Article.deleteMany();
  await Article.insertMany(seedData.articles);

  await db.disconnect();

  res.status(200).json({message: 'Process completed successfully'});
}